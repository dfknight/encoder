encoder
=======
What is it? 
-------
rencoder.py is a small script file used (in conjunction with HandBrakeCLI.exe and ffmpeg.exe) to encode multiple video files located within a directory and/or subdirectories. 

NOTE: encoder.py is no longer being developed. Please use rencoder.py.

How do I use it?
-------
First ensure that the location of HandBrakeCLI.exe is the same it is in your system. It is located on the line below "# NOTE: HB_LOCATION MUST BE SPECIFIED HERE". Once the correct location for HandBrakeCLI.exe has been entered, you can use the following parameters (in any order).
Second, ensure that ffmpeg.exe is located in the same location as encoder.py, in a subdirectory called bin.

- src
    This is the source directory that will be scanned. 
    NOTE: Individual files cannot be entered into the application as a source directory. 
    
        eg. rencoder.py src=files

- dst
    This is the destination directory. If it is specified, the location will be used as the destination directory. Otherwise, the default location of src/encodes will be used.
    NOTE: Individual files cannot be entered into the application as a destination directory. 
    
        eg. rencoder.py src=files dst=files/encodes

- quality = HD | SD | (int)
    There are 2 quality profiles stored within the application. HD will attempt to lower the quality, in an effort to preserve file space, while SD will attempt to increase the quality in exchange for increased file space.
    
        eg. rencoder.py quality=HD src=file.mkv dst=file.mp4

- recursive
    This option will scan a directory recursively, select any valid files which have not been encoded already, and encode the selected files.
    
        eg. rencoder.py src=directory recursive

        directory
        directory/file1.avi
        directory/file2.avi
        directory/director2/file3.mkv
        directory/director3/file4.mkv

        The list of files that will be encoded will be the following: 
        - file1.avi
        - file2.avi
        - file3.mkv
        - file4.mkv

- no-subs
    By default, subtitle track #1 will be encoded as a hardsub on the encoded video. The no-subs option is used in order to avoid encoded subtitles.
    
        eg. rencoder.py src=<filename or directory> no-subs

- hb
    Specify the location where HandBrakeCLI.exe is located.

        eg. rencoder.py src=<filename or directory> hb="C:\\Program Files\\Handbrake\\HandBrakeCLI.exe"

- ffmpeg
    Specify the location where ffmpeg.exe is located.

        eg. rencoder.py src=<filename or directory> ffmpeg="bin\\ffmpeg.exe"

- mkvmerge
    Specify the location where mkvmerge.exe is located. The default location for mkvmerge.exe is bin\mkvmerge.exe
    NOTE: merge must be specified as a parameter in order for mkvmerge to execute.

        eg. rencoder.py src=<filename or directory> mkvmerge="bin\\mkvmerge.exe"

- merge
    Automatically merge any files that have multiple titles in them. 
    NOTE: A valid location for mkvmerge.exe must be specified using the mkvmerge parameter.

        eg. rencoder.py src=<filename or directory> mkvmerge="bin\\mkvmerge.exe" merge