from distutils.core import setup
import glob
import py2exe

setup(
    console= {
        "rencoder.py"
    },
    options= {
        "py2exe": {
            "unbuffered": True,
            "optimize": 2,
            "bundle_files": 1,
            "ascii": False
        }
    })
