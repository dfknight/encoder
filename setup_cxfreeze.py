import sys
import os
from cx_Freeze import setup, Executable


setup(
    name="Encoder",
    version="0.1.1.2",
    description="Recursive video encoder",
    executables=[Executable("rencoder.py")],
    options = {
        "build_exe": {
            "includes": ["os", "shutil", "math", "sys", "subprocess", "re", "time"],
            "excludes": [],
            "packages": ["win32api"],
            "create_shared_zip": False,
                # don't generate Library.zip
            "append_script_to_exe": True,
                # don't generate MyApp.zip file.
            "compressed": False,
            "copy_dependent_files": True,
            "include_in_shared_zip": False,
            "optimize": 2,
            "include_msvcr": True
            }            
        }
)
