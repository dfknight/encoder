import math
import os
import os.path
from os import chdir,listdir,mkdir,remove,walk
from os.path import abspath,dirname,exists,isdir,isfile,join
from subprocess import call
import subprocess
from sys import argv,stdout,exit
import sys
import re
import time
import shutil
import multiprocessing

import win32api,win32process,win32con
import ctypes

QUALITY_NONE=0
QUALITY_HD=23
QUALITY_SD=18

STRING_EMPTY=0

default_location=dirname(abspath(sys.argv[0]))

current_location=default_location
encodes_location=None

extensions=["mkv","iso","img","avi","rmvb","wmv"]
valid_extensions=["mkv","iso","img","avi","mp4","rmvb","wmv","ogm"]
recursive=False
subtitles=True
merge=False
quality=QUALITY_NONE
vcodec="x264"
overwrite=False
quick=False

merge_check=False

remove_old_encodes=False

single_core=False
low_priority=False

log_files=True
log_location=default_location+"\\logs\\"

atrack=1

hb_location="C:\\Program Files\\Handbrake\\HandBrakeCLI.exe"
ffmpeg_location="bin\\ffmpeg.exe"
mkvmerge_location="bin\\mkvmerge.exe"

# If 0 parameters have been specified, something went wrong with python!
if(len(argv)==0):
    sys.exit("There were 0 arguments in sys.argv. That is impossible in python (as far as I'm aware)! Something must have gone wrong! Please contact an admin ASAP!")

# If 1 parameter has been specified the program ran successfully! The user had no parameters, so default to the location where the script is located.
elif(len(argv)==1):
    print("You have specified no parameters. Using the default directory of default_location("+default_location+") as the current_location.")
# If 2 parameters have been specified the user has entered a directory. Use it as the current_directory
elif(len(argv)>1):
    print("You have specified "+str(len(argv)-1)+" parameter(s).")
    for i in argv:
        if(i.split("=",1)!=None):
            if(i.split("=",1)[0]=="ext"):
                ext=i.split("=",1)[1].split(",")
                for j in ext:
                    if(str(j) in valid_extensions and not str(j) in extensions): 
                        extensions.append(str(j))
                    else:
                        print("Invalid containers detected: "+str(j))
            if(i.split("=",1)[0]=="flags"):
                flags=i.split("=",1)[1]
                if(len(flags)==1):
                    # remove_old_encodes
                    if(int(flags[0])==0):
                        remove_old_encodes=True
                    elif(int(flags[1])==1):
                        remove_old_encodes=False
                    else:
                        print("Invalid value for remove_old_encodes flag. Using default value of "+str(remove_old_encodes))
                else:
                    print("Invalid number of flags! Check documentation, or contact an administrator, and try again!")
            elif(i.split("=",1)[0]=="src"):
                current_location=i.split("=",1)[1]
            elif(i.split("=",1)[0]=="dst"):
                encodes_location=i.split("=",1)[1]
            elif(i.split("=",1)[0]=="log" and i.split("=",1)[1]=="NO"):
                log_files=False
            elif(i.split("=",1)[0]=="quality"):
                if(i.split("=",1)[1]=="SD"):
                    quality=QUALITY_SD
                elif(i.split("=",1)[1]=="HD"):
                    quality=QUALITY_HD
                else:
                    quality=int(i.split("=",1)[1])
            elif(i.split("=",1)[0]=="recursive"):
                recursive=True
            elif(i.split("=",1)[0]=="no-subs"):
                subtitles=False
            elif(i.split("=",1)[0]=="hb"):
                hb_location=i.split("=",1)[1]
            elif(i.split("=",1)[0]=="ffmpeg"):
                ffmpeg_location=i.split("=",1)[1]
            elif(i.split("=",1)[0]=="mkvmerge"):
                mkvmerge_location=i.split("=",1)[1]
            elif(i.split("=",1)[0]=="merge"):
                merge=True
            elif(i.split("=",1)[0]=="no-mp"):
                single_core=True
            elif(i.split("=",1)[0]=="low-priority"):
                low_priority=True
            elif(i.split("=",1)[0]=="x265"):
                vcodec="x265"
            elif(i.split("=",1)[0]=="overwrite"):
                overwrite=True
            elif(i.split("=",1)[0]=="audio-track"):
                atrack=int(i.split("=",1)[1])
            elif(i.split("=",1)[0]=="quick"):
                quick=True

os.chdir(default_location)
# Preliminary checks
if(merge):
    if(mkvmerge_location==""):
        print("ERROR: Location of mkvmerge not defined!")
        sys.exit(1)
    elif(not(isfile(mkvmerge_location))):
        print("ERROR: mkvmerge does not point to a file!")
        sys.exit(1)
    else:
        merge_check=True

if(ffmpeg_location==""):
    print("ERROR: Location of ffmpeg not defined!")
    sys.exit(1)
elif(not(isfile(ffmpeg_location))):
    print("ERROR: ffmpeg does not point to a file!")
    sys.exit(1)

if(hb_location==""):
    print("ERROR: Location of HandBrakeCLI not defined!")
    sys.exit(1)
elif(not(isfile(hb_location))):
    print("ERROR: HandBrakeCLI does not point to a file!")

# Check to see if the current directory exists or not
if(exists(current_location)==False):
    print("ERROR: Source path does not exist!")
    sys.exit(1)

if(not(exists("logs"))):
    mkdir("logs");

def getallfiles(path,recursive):
    list_files=[]
    if(recursive==True):
        for root, dirs, files in walk(path):
            for dirname in dirs:
                if(isdir(str(join(root,dirname,"VIDEO_TS")))):
                    dirname=str(join(root,dirname,"VIDEO_TS").encode(stdout.encoding,'replace'))[2:-1]
                    titles,subs=getMediaInfo(dirname)
                    checked_titles=1
                    while(checked_titles<titles):
                        if(not(isfile(abspath(dirname)+"\\encodes\\"+abspath(filename).rsplit("\\",1)[1]+"-"+str(checked_titles)+".mp4"))):
                            break
                        elif(checked_titles==titles):
                            break
                        else:
                            checked_titles=checked_titles+1
                    if(checked_titles!=titles):
                        list_files.append(dirname.replace('\\\\','\\'))
            for filename in files:
                if(abspath(str(join(root,filename)))=="VIDEO_TS"):
                    continue
                if(filename.rsplit(".",1)[1].lower() in extensions ):
                    filename=str(join(root,filename))
                    filename=filename.replace('\\\\','\\')
                    if(encodedFileExists(filename)):
                        continue
                    list_files.append(filename.replace('\\\\','\\'))
    else:
        for filename in listdir(path):
            if(isfile(join(path,filename)) and filename.rsplit(".",1)[1].lower() in extensions):
                filename=str(join(path,filename).encode(stdout.encoding,'replace'))[2:-1]
                filename=filename.replace('\\\\','\\')
                if(encodedFileExists(filename)):
                    continue
                list_files.append(filename.replace('\\\\','\\'))
            elif(isdir(path+'\\'+filename) and isdir(path+'\\'+filename+"\\VIDEO_TS")):
                filename=str(join(path,filename,"VIDEO_TS").encode(stdout.encoding,'replace'))[2:-1]
                titles,subs=getMediaInfo(filename)
                checked_titles=1
                while(checked_titles<titles):
                    if(not(isfile(abspath(filename)+"\\encodes\\"+abspath(filename).rsplit("\\",1)[1]+"-"+str(checked_titles)+".mp4"))):
                        break
                    elif(checked_titles==titles):
                        break
                    else:
                        checked_titles=checked_titles+1
                if(checked_titles!=titles):
                    list_files.append(filename.replace('\\\\','\\'))
    return list_files

# Get the list of files (recursively) in the specified path
def getfiles(path,recursive):
    hd_files=[]
    sd_files=[]
    if(recursive==True):
        for root, dirs, files in walk(path):
            for dirname in dirs:
                if(dirname=="VIDEO_TS"):
                    dirname=str(join(root,dirname).encode(stdout.encoding,'replace'))[2:-1]
                    dirname=dirname.replace('\\\\','\\')
                    list_files.append(dirname)
            for filename in files:
                if(filename.rsplit(".",1)[1].lower() in extensions):
                    filename=str(join(root, filename).encode(stdout.encoding,'replace'))[2:-1]
                    if(encodedFileExists(filename)):
                        continue
                    quality=getQuality(filename)
                    if(quality==QUALITY_HD):
                        hd_files.append(filename.replace('\\\\','\\'))
                    elif(quality==QUALITY_SD or quality==QUALITY_NONE):
                        sd_files.append(filename.replace('\\\\','\\'))
    else:
        files=listdir(path)
        if(len(files)>0):
            for filename in files:
                if(isfile(path+'\\'+filename) and filename.rsplit(".",1)[1].lower() in extensions):
                    filename=str(join(path, filename).encode(stdout.encoding,'replace'))[2:-1]
                    if(encodedFileExists(filename)):
                        continue
                    quality=getQuality(filename)
                    if(quality==QUALITY_HD):
                        hd_files.append(filename.replace('\\\\','\\'))
                    elif(quality==QUALITY_SD or quality==QUALITY_NONE):
                        sd_files.append(filename.replace('\\\\','\\'))
    return hd_files,sd_files

pattern=re.compile(r'Stream.*Video.*, ([0-9]+)x([0-9]+)')

# Check the quality of the file using ffmpeg.exe
def getQuality(filename):
    quality=QUALITY_NONE
    call_args=[ffmpeg_location,"-i",filename]
    
    p=subprocess.Popen(call_args,stderr=subprocess.PIPE,stdout=open(os.devnull))
    out, err = p.communicate()
    
    match=pattern.search(err.decode('utf-8'))
    if(match):
        x,y=map(int, match.groups()[0:2])
    else:
        x=y=0

    # Determine if the quality of the video is HD or SD based on the number of pixels
    # per frame. Information retrieved from: 
    # http://en.wikipedia.org/wiki/List_of_common_resolutions
    if(x*y>=921600):
        quality=QUALITY_HD
    else:
        quality=QUALITY_SD
    return quality

def getSeconds(filename):
    call_args=[ffmpeg_location,"-i",filename]

    p=subprocess.Popen(call_args, stderr=subprocess.PIPE, stdout=open(os.devnull))
    out, err = p.communicate()

    if(os.path.basename(filename) != "VIDEO_TS"):
        if("Duration" in err.decode('latin-1')):
            time_str=str(err[err.decode('latin-1').index("Duration"):].split()[1]).split(':');
            time_hours=time_str[0]
            time_minutes=time_str[1]
            time_seconds=time_str[2].split('.')[0]
            time_total=int(time_hours[2:len(time_hours)])*60*60
            time_total=time_total + int(time_minutes)*60
            time_total=time_total + int(time_seconds)
            return time_total
    return -1

def getBytes(filename):
    try:
        statinfo = os.stat(filename)
        return int(statinfo.st_size)
    except FileNotFoundError:
        return -1

def getLogCode(filename):
    with open(filename, encoding='latin-1') as f:
        found=False
        lines=f.readlines();
        for line in lines:
            if("libhb: work result" in line):
                found=True
                return int(line.split(' ')[-1])
    return int(-1)
        
# Check to see if the file has already been encoded or not
# Also check to see if the encoded file has the same duration as the
# source file
def encodedFileExists(filename):
    if(not(exists(filename.rsplit("\\",1)[0]+"\\encodes\\"+filename.rsplit("\\",1)[1].rsplit(".",1)[0]+".mp4")) and not(exists(filename.rsplit(".")[0]+".mp4"))):
        return False
    elif(exists(filename.rsplit("\\",1)[0]+"\\encodes\\"+filename.rsplit("\\",1)[1].rsplit(".",1)[0]+".mp4") or exists(filename.rsplit(".")[0]+".mp4")):
        pattern=re.compile(r'Duration: .*')
        call_args=[ffmpeg_location,"-i",filename]
        p=subprocess.Popen(call_args,stderr=subprocess.PIPE,stdout=open(os.devnull))
        out,err=p.communicate()

        call_args=[ffmpeg_location,"-i",filename.rsplit("\\",1)[0]+"\\encodes\\"+filename.rsplit("\\",1)[1].rsplit(".",1)[0]+".mp4"]
        p=subprocess.Popen(call_args,stderr=subprocess.PIPE,stdout=open(os.devnull))
        out2,err2=p.communicate()

        match=pattern.search(err.decode('utf-8'))
        match2=pattern.search(err2.decode('utf-8'))
        if(match and match2):
            test=''.join(map(str,match.group()))
            duration=test.split(' ',1)[1].split(',',1)[0].split(':')
            seconds=int(duration[1])*60+int(duration[2].split(".",1)[0])

            test2=''.join(map(str,match2.group()))
            duration2=test2.split(' ',1)[1].split(',',1)[0].split(':')
            seconds2=int(duration2[1])*60+int(duration2[2].split(".",1)[0])
            if(math.fabs(seconds-seconds2)<=1):
                return True
        return False
    return True
# Check to see if the files have already been encoded or not
def parseEncodedFiles(original_list):
    tmp_files=[]
    for f in original_list:
        if(encodes_location==None):
            encode_location=f.rsplit("\\",1)[0]+"\\encodes\\"
        else:
            encode_location=encodes_location
        if(not(encodedFileExists(f))):
            tmp_files.append(f)
    return tmp_files

# Encode the current file using HandBrakeCLI.exe
def encode(files,quality):
    counter = 0
    for f in files:
        print("")
        prev_bytes=int(getBytes(f))
        prev_time=int(getSeconds(f))
        counter = counter + 1
        success=False
        if(encodedFileExists(f) and f.rsplit('\\',1)[1]!="VIDEO_TS"):
            print("Encoded file ["+str(f.encode('utf-8'))[2:-1]+"] already exists!")
            continue
        if(encodes_location==None):
            if(f.rsplit('\\',1)[1]=="VIDEO_TS"):
                encode_location=join(f.rsplit('\\',2)[0],"encodes")
            else:
                encode_location=join(f.rsplit('\\',1)[0],"encodes")
        else:
            encode_location=encodes_location
            
        if(not(exists(encode_location))):
            try:
                mkdir(encode_location)
            except:
                print("Unable to create encode directory! "+encode_location)
                continue
        
        print("Currently encoding ("+str(counter)+"/"+str(len(files))+"): ",end='')
        if(f.rsplit('\\',1)[1]=="VIDEO_TS"):
            print(str(f.rsplit('\\',1)[0].rsplit('\\',1)[1].encode('utf-8'))[2:-1])
        else:
            print(str(f.rsplit("\\",1)[1].encode('utf-8'))[2:-1])
        stdout.flush()

        if(prev_bytes==-1):
            print("Unable to get the size of the file!")
            continue
        elif(prev_time==-1 and os.path.basename(f)!="VIDEO_TS"):
            print("Unable to get the duration of the video!")
            continue
        
        title_to_encode,embeddedsubs=getMediaInfo(f)
        if(title_to_encode==None):
            print("Unable to encode file: "+f)
            continue
        if sys.platform.startswith("win"):
            SEM_NOPAGEFAULTERRORBOX = 0x0002
            ctypes.windll.kernel32.SetErrorMode(SEM_NOPAGEFAULTERRORBOX)
            CREATE_NO_WINDOW = 0x08000000
            subprocess_flags = CREATE_NO_WINDOW
        else:
            subprocess_flags = 0
        for x in range(1,title_to_encode+1):
            call_args=[hb_location,"-i",f,"-q",str(quality),"-e",vcodec,"-a",str(atrack),"-E","copy","--audio-fallback","ffaac"]
            
            if(low_priority):
                call_args.append("-x")
                call_args.append("threads="+str(multiprocessing.cpu_count()-1)+":")
            if(single_core):
                call_args.append("-x")
                call_args.append("threads=1:")
            if(subtitles):
                if(exists(f.rsplit(".",1)[0]+".srt")):
                    call_args.append("--srt-file")
                    call_args.append(f.rsplit(".",1)[0] .replace("\\","/")+".srt")
                    call_args.append("--srt-codeset")
                    call_args.append("UTF-8")
                    call_args.append("--srt-lang")
                    call_args.append("eng")
                    call_args.append("--srt-offset")
                    call_args.append("0")
                    call_args.append("--srt-default=1")
                    call_args.append("--markers")
                    appdata=os.getenv('APPDATA')
                    call_args.append(appdata.rsplit("\\",1)[0]+"\\Local\\Temp\\2\\"+f.rsplit("\\",1)[1].rsplit(".")[0]+"-1-chapters.csv")
                elif(embeddedsubs):
                    call_args.append("-s")
                    call_args.append("1")
                    call_args.append("--subtitle-burn")
            if(quick):
                call_args.append("--x264-preset")
                call_args.append("veryfast")
                
            call_args.append("-o")
            if(isdir(f)):
                if(title_to_encode==1):
                    call_args.append(join(f.rsplit('\\',2)[0],"encodes",f.rsplit('\\',1)[0].rsplit('\\',1)[1]+".mp4"))
                else:
                    call_args.append(join(f.rsplit('\\',2)[0],"encodes",f.rsplit('\\',1)[0].rsplit('\\',1)[1]+"-"+str(x)+".mp4"))
                    call_args.append("-t")
                    call_args.append(str(x))
            else:
                if(title_to_encode==1):
                    call_args.append(join(encode_location,f.rsplit("\\",1)[1].rsplit(".",1)[0]+".mp4"))
                else:
                    call_args.append(join(encode_location,f.rsplit("\\",1)[1].rsplit(".",1)[0]+"-"+str(x)+".mp4"))
                    call_args.append("-t")
                    call_args.append(str(x))

            f2=open(log_location+f.rsplit("\\",1)[1]+".txt","w")
            
            p=subprocess.Popen(call_args, bufsize=-1, stdin=open(os.devnull), stdout=subprocess.PIPE, stderr=f2, creationflags=subprocess_flags)

            # Ensure that only one core is used. By default core 1 will be used.
            if(single_core or low_priority):
                handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, False, p.pid)
                if(single_core):
                    win32process.SetProcessAffinityMask(handle, 0)
                if(low_priority):
                    priorityclasses = [win32process.IDLE_PRIORITY_CLASS,
                       win32process.BELOW_NORMAL_PRIORITY_CLASS,
                       win32process.NORMAL_PRIORITY_CLASS,
                       win32process.ABOVE_NORMAL_PRIORITY_CLASS,
                       win32process.HIGH_PRIORITY_CLASS,
                       win32process.REALTIME_PRIORITY_CLASS]
                    win32process.SetPriorityClass(handle, priorityclasses[0])
                win32api.CloseHandle(handle)
            
            line=list()
            output=""
            while True:
                stdout2=p.stdout.readline(1)
                if(stdout2.decode('utf-8')!='\r'):
                    line.append(stdout2.decode('utf-8'))
                else:
                    eta_value=""
                    percent_value=""
                    output=''
                    try:
                        eta_value="".join(line).split(' ')[13].split(')')[0]
                    except:
                        pass
                    try:
                        percent_value="".join(line).split(' ')[5]
                    except:
                        pass
                    output=output+"Progress:"
                    if percent_value is not "":
                        output=output+' '+str(percent_value)+' %'
                    if eta_value is not "":
                        output=output+' ETA: '+eta_value
                    output=output+' ... '
                    
                    print(output,end='\r')
                    sys.stdout.flush()
                    
                    line=list()
                if(stdout2.decode('utf-8')=='' and p.poll()!=None):
                    break
            f2.close()

            new_bytes=int(getBytes(call_args[call_args.index('-o')+1]))
            new_time=int(getSeconds(call_args[call_args.index('-o')+1]))
            log_code=int(getLogCode(log_location+f.rsplit("\\",1)[1]+".txt"))

            if(log_code==0 or new_bytes>=prev_bytes*0.60 or abs(new_time-prev_time)<=5):
                success=True

            if(success==True):
                output="Progress: 100 % ETA: 00h00m00s"
                #if(f.rsplit('\\',1)[1]=="VIDEO_TS"):
                #    output=output+f.rsplit('\\',1)[0].rsplit('\\',1)[1]
                #else:
                #    output=output+f.rsplit('\\',1)[1]
                output=output+" ... Done!"
            else:
                output=output+"FAILED!"
            
            total_length=len(output)+27
            while(len(output)<total_length):
                output = output + " "
            print(output)
        if(not(success)):
            print("")
            print("Encode FAILED!")
            continue
        if(merge_check and title_to_encode>1):
            print("Merging files ...",end="")
            call_args=[mkvmerge_location]
            call+args.append("\""+join(f.rsplit('\\',2)[0],"encodes",f.rsplit('\\',1)[0].rsplit('\\',1)[1]+".mp4")+"\"")
            for x in range(1,title_to_encode+1):
                call_args.append("\""+join(f.rsplit('\\',2)[0],"encodes",f.rsplit('\\',1)[0].rsplit('\\',1)[1]+"-"+str(x)+".mp4")+"\"")
                if(x<=title_to_encode):
                    call_args.append("+")
            p=subprocess.Popen(call_args, bufsize=-1, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            out,err=p.communicate()
            print("Done!")
        if(overwrite):
            copy_from=call_args[call_args.index('-o')+1]
            copy_to=join(os.path.dirname(f), os.path.basename(call_args[call_args.index('-o')+1]))
            shutil.copyfile(copy_from, copy_to)
            os.remove(f)
            os.remove(copy_from)
            print("Source file has been overwritten!")
        print("")
        print("Encode SUCCEEDED!")
        #time.sleep(10)

# Get video track information of the file that is going to be encoded
def getMediaInfo(filename):
    call_args=[hb_location,"-i",filename,"-t0","-v"]
    p=subprocess.Popen(call_args,stdin=open(os.devnull),stdout=open(os.devnull),stderr=subprocess.PIPE,bufsize=-1)
    out, err = p.communicate()
    title=None
    subs=True

    data=[]
    temp_data=""
    for test in err:
        if(test==10):
            data.append(temp_data)
            temp_data=""
        else:
            temp_data=temp_data+chr(test)
    for test in data:
        if(test[0:7]=="+ title"):
            title=int(test[8:-2])
        elif(test[0:10]=="+ subtitle" and title!=None):
            subs=True
    return title,subs

# Get a list of files to be encoded
hd_files=[]
sd_files=[]

if(isfile(current_location)):
    if(quality==QUALITY_NONE):
        quality=getQuality(current_location)
    if(quality>=QUALITY_HD):
        hd_files.append(current_location)
    elif(quality<QUALITY_HD):
        sd_files.append(current_location)
elif(isdir(current_location) and current_location.rsplit("\\",1)[1]=="VIDEO_TS"):
    if(quality==QUALITY_NONE):
        quality=QUALITY_HD
    if(quality>=QUALITY_HD):
        hd_files.append(current_location)
    elif(quality<=QUALITY_SD):
        sd_files.append(current_location)
elif(isdir(current_location)):
    print("Compiling a list of files to be encoded ... ",end="")
    stdout.flush()
    if(quality==QUALITY_NONE):
        hd_files,sd_files=getfiles(current_location,recursive)
        hd_files=parseEncodedFiles(hd_files)
        sd_files=parseEncodedFiles(sd_files)
    else:
        if(quality>=QUALITY_HD):
            hd_files=parseEncodedFiles(getallfiles(current_location,recursive))
        else:
            sd_files=parseEncodedFiles(getallfiles(current_location,recursive))
    print("Done!\n")

total=len(hd_files)+len(sd_files)
if(total==0):
    print("NO FILES TO ENCODE!")
else:
    
    print("A total of "+str(len(hd_files))+" HD files will be encoded!")
    print("A total of "+str(len(sd_files))+" SD files will be encoded!")
    print("A total of "+str(total)+" files will be encoded!\n")

    hd_files.sort()
    sd_files.sort()

    if(quality==QUALITY_NONE):
        encode(hd_files,QUALITY_HD)
        encode(sd_files,QUALITY_SD)
    elif(quality>QUALITY_NONE):
        encode(hd_files,quality)
        encode(sd_files,quality)
    else:
        print("Invalid quality entered!")
    
